# Notes per a promoure la iniciativa Canal 7-7 PMR-446

Mikel L. Forcada, EA5IYL / 30FRS014 / 30RKB357

## Qualsevol persona pot participar

Qualsevol pot participar i millorar la seguretat en muntanya

* En la muntanya o en la vall
* Amb un equip PMR-446 assequible i a la venda en grans superfícies d'esports i altres botigues (a voltes com a joguet). Molta gent té _walkies_ PMR-446 però no en trau profit.
  * No tots els _walkies_ PMR-446 valen: han de tenir els 38 _subtons_ o _subcanals_ a més dels 8 o 16 canals principals.
* Amb molt poc aprenentatge es poden usar molt eficientment.

## Arriben lluny els _walkies_ PMR-446?

* Els _walkies_ PMR-446 treballen en la freqüència de 446 megahertzs de la UHF (entre 446,0 i 446,2 MHz).
* Usen una potència relativament baixa, de 0,5 W (en contrast, un equip portàtil de radioaficionat té típicament 5 W i si va muntat en un vehicle, 25 W).
* Arriben bé on la vista arriba; sense interferències, no és estrany salvar desenes de kilòmetres.
* Si hi ha obstacles (edificis, bosc espés), l'abast es redueix molt.
* És pràcticament impossible que puguem parlar amb gent a l'altre costat d'una muntanya.

## Construïm una xarxa

* Es tracta de formar una __xarxa de seguretat mútua__ basada en _walkies_ a l'escolta, situats en com més llocs millor, en les valls i en les muntanyes. __Com més gent participe en la iniciativa Canal 7-7, més efectiva serà en cas d'emergència__.
* Qui porta el _walkie_ PMR-446 pot trobar-se en diverses situacions diferents:
   * Pot no tenir cobertura de telefonia mòbil (sense accés al 112) i necessitar enviar una crida de socors només amb el _walkie_.
   * Pot tenir cobertura de telefonia mòbil (amb accés al 112) i pot atendre la crida de socors i passar les dades al 112.
      * És més fàcil tenir cobertura de telefonia mòbil en els cims (però a voltes hi ha problemes) que en les valls.
      * És més fàcil tenir cobertura de telefonia mòbil en àrees més poblades.
   * Pot no tenir cobertura de telefonia mòbil però tenir llicència de radioaficionat i usar un equip de radioaficionat per a passar les dades de la crida de socors als serveis d'emergència
      * Hi ha radioaficionats que fan activitats de ràdio en antena com "cims en antena" (Summits on the Air) o un diploma de vèrtexs geodèsics molt popular. Els _repetidors_ instal·lats pels radioclubs allarguen l'abast de les seues comunicacions.
	  * Alguns radioaficionats de l'estat espanyol són, a més, membres de REMER ("Xarxa Nacional de Ràdio de Emergència") i poden usar freqüències especials addicionals i repetidors específics.
   * No cal que siga a la muntanya. També s'hi pot participar de casa, particularment si un viu en una zona elevada, alta o lliure d'obstacles amb muntanyes a la vista.
* La xarxa serà més efectiva com més gent hi participe; de fet, la xarxa no funcionarà si no som molts els que hi participem.

## Porta el _walkie_ sempre damunt 

* El succés de la iniciativa depén de la participació.
* Com més gent hi participe, més possibilitats hi ha que una crida de socors siga escoltada i atesa.

## Com portar el _walkie_

* Abans d'anar a la muntanya, assegura't que té piles o està ben carregat.
* Si seràs molt temps en la muntanya, i el _walkie_ ho permet, porta un altre joc de piles.
* Col·locar-lo de manera que l'antena vaja lliure: un bon lloc és enganxar-lo a la part alta d'un dels tiradors de la motxilla.

## Com parlar

* Escolta sempre abans de parlar. I no parles si el canal és ocupat. 
   * Si és urgent, intenta transmetre entre _torns_ de la conversa que escoltes dient alguna cosa breu com _ajuda!_, _atenció!_ o _emergència!_. 
* Si ningú parla, agarra el _walkie_ amb la mà a un pam de la boca, amb l'antena ben vertical.
* Prem el botó de parlar (o _PTT_) i deixa passar un segon.
* Parla clarament i vocalitza. Si has de donar un nom, usa un alfabet estàndard per lletrejar (_Alfa_, _Bravo_, _Charlie_, etc. per exemple; veure més avall)
* En acabar de parlar, deixa passar un segon abans de soltar el botó de parlar (o _PTT_).
* Si l'equip emet un to o bip al final de la transmissió, no cal que digues _canvi_ al final.

## Protocol general

* Quan sigues en la muntanya, a una certa altura, __transmet un breu missatge de tant en tant__ en el canal 7-7:
   * Digues qui eres (el teu nom, per exemple _Marta_, o un indicatiu que hages elegit, per exemple _Cabreta_ o _30FRS013_).
   * Indica aproximadament on et trobes.
   * Indica quants sou i què feu.
   * Pot ser útil dir si tens cobertura de mòbil.
   * Digues que transmets en el canal 7-7 (és important que sàpiguen que uses el _subcanal_ 7).
   * Per exemple, digues:
      *  _Atent, canal 7, subcanal 7, canal de ràdio en muntanya. Transmet Cabreta, pujant a la Fenessosa per l'est. Vaig sola i de moment tinc cobertura de mòbil_, o 
      * _Bon dia canal 7, subcanal 7, sóc Marta i vaig en un grup de quatre persones pel pas de la Rabosa en Aitana_, o 
      * _Atenció canal 7, subcanal 7, sóc Xavi. Som tres companys pujant per la via ferrata Canelobre--Cabeso d'Or._
   * Pots afegir també informació d'interés: _Hi ha una boira molt espessa_ o _Hi ha un fort vent del nord-est_.
* Quan escoltes algú, __transmet un breu missatge i confirma-li que l'escoltes__. Això fa que tothom qui participa sàpiga que té una miqueta més de seguretat.
   * Per exemple, _Bon dia, Marta. Sóc trenta foxtrot romeo sierra zero tretze i en la font de l'Arbre t'escolte molt bé pel canal 7-7.__.
* __No ocupes el canal innecessàriament.__ Si has de mantenir una conversa, deixa que passe un parell de segons entre _torns_, per si algú necessita comunicar-s'hi.

## En cas d'accident

(pres de Bàez García i coautors, veure _Lectures Interessants_ més avall).

* Amb el canal 7-7 lliure, transmetre el missatge
  *  _Atenció, crida d'emergència. Atenció, crida d'emergència. Algú m'escolta en el Canal 7 - Subcanal 7?_
* Si no rebem resposta, enviar periòdicament un missatge de l'estil de: 
  *  _Atenció, crida d'emergència a través del Canal 7, subcanal 7. Atenció, crida d'emergència a través del Canal 7, subcanal 7. El meu nom és Marta. Necessitem assistència sanitària urgent per_ (motiu). _No tenim contacte amb el 112. La nostra posició és_ (localització, coordenades, etc.). 
  * Esperar un o dos minuts entre missatges. Si cal, canvia de canal o elimina el subto.

Si hi ha més persones escoltant, la comunicació amb els serveis d'emergència és ara possible.


## Alfabet per a lletrejar

| Lletra | Nom ICAO | Pronúncia aproximada |
| ------ | -------- | -------------------- |
| A | alpha | àl-fa |
| B | bravo | brà-vou |
| C | Charlie | xàr-li |
| D | delta | dèl-ta |
| E | echo | è-kou, è-ko |
| F | foxtrot | fòx-trot |
| G | golf | golf |
| H | hotel | hou-tèl, ho-tèl |
| I | India | ín-di-a |
| J | Juliette | jú-li-et |
| K | kilo | quí-lou, quí-lo |
| L | Lima | lí-ma |
| M | Mike | maik |
| N | November | no-vèm-bar, no-vèm-ber |
| O | Oscar | òs-kar |
| P | papa | pà-pa |
| Q | Quebec | qüe-bèc |
| R | Romeo | róu-mi-ou, rò-me-o |
| S | sierra | Si-è-rra |
| T | tango | tàn-go, tàn-gou | 
| U | uniform | iú-ni-form |
| V | Victor | vík-tor |
| W | whisky | uís-ki |
| X | X-ray | èks-rei |
| Y | Yankee | iàn-ki |
| Z | Zulu | zú-lu |

## Lectures interessants

* Ballesteros Peña, Sendoa (2022) [Canal 7-7 PMR: Cómo un ‘walkie talkie’ de juguete podría salvarnos la vida](https://theconversation.com/canal-7-7-pmr-como-un-walkie-talkie-de-juguete-podria-salvarnos-la-vida-152592), en _The Conversation_.
* Báez García, Axier; Ballesteros Peña, Sendoa; Izquierdo Valdueza, Ander (visitada el 05/01/2022)
[Telecomunicaciones de emergencia en la montaña: cómo comunicarse en zonas sin cobertura de telefonía móvil](https://www.barrabes.com/blog/consejos/2-10240/telecomunicaciones-emergencia-montana-como-comunicarse), epígrafe 2.1.
* Brito, Adrián (2016) [Uso de los radiotransmisores PMR-446: La iniciativa Canal 7 Subtono 7](https://www.femecv.com/sites/default/files/boletin_2016_no_14_actualizado_2016.04.25.pdf), FEMECV: Boletín Informatiu de la Federació d'Esports de Muntanya i Escalada de la Comunitat Valenciana, núm. 14, pàgs. 46-51.
* Forcada, Mikel (2021) [«Canal 7-7»: Et poden salvar la vida uns «walkie talkies» de joguina?](https://www.nosaltreslaveu.cat/noticia/65691/canal-7-7-et-poden-salvar-la-vida-uns-walkie-talkies-de-joguina), en _Nosaltres la Veu_.
 
