# Notas para promover la iniciativa Canal 7-7 PMR-446

Mikel L. Forcada, EA5IYL / 30FRS014 / 30RKB357

## Cualquier persona puede participar

Cualquiera puede participar y mejorar la seguridad en montaña

* En la montaña o en el valle
* Con un equipo PMR-446 asequible y a la venta en grandes superficies de deportes y otras tiendas (a veces como juguete). Mucha gente tiene _walkies_ PMR-446 pero no les saca provecho.
   * No todos los _walkies_ PMR-446 valen: deben tener los 38 _subtonos_ o subcanales __ además de los 8 o 16 canales principales.
* Con muy poco aprendizaje se pueden usar muy eficientemente.

## ¿Llegan lejos los _walkies_ PMR-446?

* Los _walkies_ PMR-446 trabajan en la frecuencia de 446 megahercios de la UHF (entre 446,0 y 446,2 MHz).
* Usan una potencia relativamente baja, de 0,5 W (en contraste, un equipo portátil de radioaficionado tiene típicamente 5 W y si va montado en un vehículo, 25 W).
* Llegan bien donde la vista llega; sin interferencias, no es extraño salvar decenas de kilómetros.
* Si hay obstáculos (edificios, bosque espeso), el alcance se reduce mucho.
* Es prácticamente imposible que podamos hablar con gente al otro lado de una montaña.

## Construyamos una red

* Se trata de formar una __red de seguridad mutua__ basada en _walkies_ a la escucha, situados en cuanto más lugares mejor, en los valles y en las montañas. __Cuanta más gente participe en la iniciativa Canal 7-7, más efectiva será en caso de emergencia__.
* Quien lleva el _walkie_ PMR-446 puede encontrarse en varias situaciones diferentes:
   * Puede no tener cobertura de telefonía móvil (sin acceso al 112) y necesitar enviar una llamada de socorro sólo con _el walkie_.
   * Puede tener cobertura de telefonía móvil (con acceso al 112) y puede atender la llamada de socorro y pasar los datos al 112.
      * Es más fácil tener cobertura de telefonía móvil en las cumbres (pero a veces hay problemas) que en los valles.
      * Es más fácil tener cobertura de telefonía móvil en áreas más pobladas.
   * Puede no tener cobertura de telefonía móvil pero tener licencia de radioaficionado y usar un equipo de radioaficionado para pasar los datos de la llamada de socorro a los servicios de emergencia
      * Hay radioaficionados que hacen actividades de radio en antena como "cumbres en antena" (Summits on the Air) o un diploma de vértices geodésicos muy popular. Los _repetidores_ instalados por los radioclubs alargan el alcance de sus comunicaciones.
      * Algunos radioaficionados del estado español son, además, miembros de REMER ("Red Nacional de Radio de Emergencia") y pueden usar frecuencias especiales adicionales y repetidores específicos.
   * No hace falta que esté en la montaña. También se  puede participar de casa, particularmente si uno vive en una zona elevada, alta o libre de obstáculos con montañas a la vista.
* La red será más efectiva cuanto más gente  participe; de hecho, la red no funcionará si no somos muchos los que  participamos.

## Lleva el _walkie_ siempre encima

* El èxit de la iniciativa depende de la participación.
* Cuanta más gente  participe, más posibilidades hay  de que una llamada de socorro sea escuchada y atendida.

## Como llevar el _walkie_

* Antes de ir a la montaña, asegúrate de que tiene pilas o está bien cargado.
* Si vas a estar mucho tiempo en la montaña, y el _walkie_ lo permite, llévate otro juego de pilas.
* Colocarlo de forma que la antena vaya libre: un buen lugar es fijarlo en lo alto de uno de los tiradores de la mochila.

## Como hablar

* Escucha siempre antes de hablar. Y no hablas si el canal está ocupado.
   * Si es urgente, intenta transmitir entre _turnos_ de la conversación que escuchas diciendo algo breve como _¡ayuda!_, _¡atención!_ o ¡emergencia! __.
* Si nadie habla, coge el _walkie_ con la mano a un palmo de la boca, con la antena bien vertical.
* Pulsa el botón de hablar (o PTT __) y deja pasar un segundo.
* Parla claramente y vocaliza. Si tienes que dar un nombre, usa un alfabeto estándar para deletrear (_Alfa_, _Bravo_, _Charlie_, etc. por ejemplo; ver más abajo)
* Al acabar de hablar, deja pasar un segundo antes de soltar el botón de hablar (o PTT __).
* Si el equipo emite un tono o bip al final de la transmisión, no hace falta que digas _cambio_ al final.

## Protocolo general

* Cuando estés en la montaña, a una cierta altura, __transmite un breve mensaje de vez en cuando__ en el canal 7-7:
   * Di quién eres (tu nombre, por ejemplo _Marta_, o un indicativo que hayas elegido, por ejemplo _Cabrita_ o 30FRS013 __).
   * Indica aproximadamente donde te encuentras.
   * Indica cuántos sois y qué haceis.
   * Puede ser útil decir si tienes cobertura de móvil.
   * Di que transmites en el canal 7-7 (es importante que sepan que usas el _subcanal_ 7).
   * Por ejemplo, di:
      * _Atento, canal 7, subcanal 7, canal de radio en montaña. Transmite Cabrita, subiendo al Fenessosa por el este. Voy sola y de momento tengo cobertura de móvil_, o
      * _Buenos días canal 7, subcanal 7, soy Marta y voy en un grupo de cuatro personas por el pas de la Rabosa en Aitana_, o
      * _Atención canal 7, subcanal 7, soy Xavi. Somos tres compañeros subiendo por la vía ferrata Canelobre--Cabeso d'Or._
   * Puedes añadir también información de interés: _Hay una niebla muy espesa_ o _Hace un fuerte viento del nordeste_.
* Cuando escuches alguien, __transmite un breve mensaje y confírmale que le escuchas.__. Esto hace que todos los que participan sepan que tiene un poco más de seguridad.
   * Por ejemplo, _Buenos días, Marta. Soy treinta foxtrot romeo sierra cero trece y en la font de l'Arbre te escucho muy bien por el canal 7-7.__.
* __No ocupes el canal innecesariamente.__ Si debes mantener una conversación, deja que pase un par de segundos entre _turnos_, por si alguien necesita comunicarse.

## En caso de accidente

(tomado de Bàez García y coautors, ver _Lecturas Interesantes_ más abajo).

* Con el canal 7-7 libre, transmitir el mensaje
   * _Atención, llamada de emergencia. Atención, llamada de emergencia. ¿Me escucha alguien en el Canal 7 - Subcanal 7?_
* Si no recibimos respuesta, enviar periódicamente un mensaje del estilo de:
   * _Atención, llamada de emergencia a través del Canal 7, subcanal 7. Atención, llamada de emergencia a través del Canal 7, subcanal 7. Mi nombre es Marta. Necesitamos asistencia sanitaria urgente por_ (motivo). _No tenemos contacto con el 112. Nuestra posición es_ (localización, coordenadas, etc.).
   * Esperar uno o dos minutos entre mensajes. Si hace falta, cambia de canal o elimina el subtono.

Si hay más personas escuchando, la comunicación con los servicios de emergencia es ahora posible.


## Alfabeto para deletrear

| Letra | Nombre ICAO | Pronunciación aproximada |
| ------ | -------- | -------------------- |
| A | alpha | ál-fa |
| B | bravo | brá-vou |
| C | Charlie | chár-li |
| D | delta | dél-ta |
| E | echo | é-kou, é-ko |
| F | foxtrot | fóx-trot |
| G | golf | golf |
| H | hotel | hou-tél, ho-tél |
| I | India | ín-di-a |
| J | Juliette | yú-li-et |
| K | kilo | quí-lou, quí-lo |
| L | Lima | lí-ma |
| M | Mike | maik |
| N | November | no-vém-bar, no-vém-ber |
| O | Oscar | ós-kar |
| P | papa | pà-pa |
| Q | Quebec | qüe-béc |
| R | Romeo | róu-mi-ou, ró-me-o |
| S | sierra | Si-é-rra |
| T | tango | tán-go, tán-gou |
| U | uniform | yú-ni-form |
| V | Victor | vík-tor |
| W | whisky | güís-ki |
| X | X-ray | éks-rei |
| Y | Yankee | yán-ki |
| Z | Zulu | sú-lu |

## Lecturas interesantes

* Ballesteros Peña, Sendoa (2022) [Canal 7-7 PMR: Cómo un ‘walkie talkie’ de juguete podría salvarnos la vida](https://theconversation.com/canal-7-7-pmr-como-un-walkie-talkie-de-juguete-podria-salvarnos-la-vida-152592), en _The Conversation_.
* Báez García, Axier; Ballesteros Peña, Sendoa; Izquierdo Valdueza, Ander (visitada el 05/01/2022)
   [Telecomunicaciones de emergencia en la montaña: cómo comunicarse en zonas sin cobertura de telefonía móvil](https://www.barrabes.com/blog/consejos/2-10240/telecomunicaciones-emergencia-montana-como-comunicarse), epígrafe 2.1.
* Brito, Adrián (2016) [Uso de los radiotransmisores PMR-446: La iniciativa Canal 7 Subtono 7](https://www.femecv.com/sites/default/files/boletin_2016_no_14_actualizado_2016.04.25.pdf), FEMECV: Boletín Informatiu de la Federació d'Esports de Muntanya i Escalada de la Comunitat Valenciana, núm. 14, pàgs. 46-51.
* Forcada, Mikel (2021) [«Canal 7-7»: Et poden salvar la vida uns «walkie talkies» de joguina?](https://www.nosaltreslaveu.cat/noticia/65691/canal-7-7-et-poden-salvar-la-vida-uns-walkie-talkies-de-joguina), en _Nosaltres la Veu_.

