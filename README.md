# Canal-7-7-pmr

## Descripció / descripció

Documents per a promoure la iniciativa Canal 7-7 en PMR 446

Documentos para promover la inicaitiva Canal 7-7 en PMR 446

## Llicència / licencia

[CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

## Contribucions / Contribuciones

Enviar una "issue" o un "merge request".

## Autors / autores

Mikel L. Forcada EA5IYL / 30FRS014

## Project status

En progreso / en progrés
